//
//  main.m
//  TTRS Tool
//
//  Created by Prachaya Pan-in on 10/16/2558 BE.
//  Copyright © 2558 Prachaya Pan-in. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
