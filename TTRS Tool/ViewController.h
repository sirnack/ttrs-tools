//
//  ViewController.h
//  TTRS Tool
//
//  Created by Prachaya Pan-in on 10/16/2558 BE.
//  Copyright © 2558 Prachaya Pan-in. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController{
    
    IBOutlet UILabel *currentDate;
    IBOutlet UIButton *locateButt;
    
}

- (IBAction)getMap:(id)sender;

@end

