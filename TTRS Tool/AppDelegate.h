//
//  AppDelegate.h
//  TTRS Tool
//
//  Created by Prachaya Pan-in on 10/16/2558 BE.
//  Copyright © 2558 Prachaya Pan-in. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

