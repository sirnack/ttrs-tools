//
//  MapLocateViewController.m
//  TTRS Tool
//
//  Created by Prachaya Pan-in on 10/20/2558 BE.
//  Copyright © 2558 Prachaya Pan-in. All rights reserved.
//

#import "MapLocateViewController.h"

#import <GoogleMaps/GoogleMaps.h>

#import <CoreLocation/CoreLocation.h>

#import "ViewController.h"
@interface MapLocateViewController ()<GMSMapViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *gmview;
@property (nonatomic) GMSMarker *markers;
@property (nonatomic) CLLocation *tempLocation;
@end

@implementation MapLocateViewController {
    GMSMapView *mapView_;
    BOOL firstLocationUpdate_;
}
@synthesize mview,gmsview,label_point,view_place;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self getMap];
    self.remove.hidden = YES;
    
}

- (IBAction)closemap:(id)sender{
    [self dismissModalViewControllerAnimated:YES];
}


- (IBAction)removepin:(id)sender {
    
    [mapView_ clear];
    self.remove.hidden = YES;
}

- (void)getMap{
    
    CLLocationManager *locationManager = [[CLLocationManager alloc] init];
    if ([locationManager locationServicesEnabled])
    {
        locationManager.distanceFilter = 5.0f;
        [locationManager startUpdatingLocation];
    }
    
    CLLocation *location = [locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:coordinate.latitude
                                                            longitude:coordinate.longitude
                                                                 zoom:15];
    
    mapView_ = [GMSMapView mapWithFrame:gmsview.bounds camera:camera];
    mapView_.settings.compassButton = NO;
    mapView_.settings.myLocationButton = NO;
    
    
    // Listen to the myLocation property of GMSMapView.
    [mapView_ addObserver:self
               forKeyPath:@"myLocation"
                  options:NSKeyValueObservingOptionNew
                  context:NULL];
    

    
    [gmsview insertSubview:mapView_ atIndex:0];
     mapView_.delegate = self;
    
    // Ask for My Location data after the map has already been added to the UI.
    dispatch_async(dispatch_get_main_queue(), ^{
        mapView_.myLocationEnabled = YES;
    });
    
}


- (void)mapView:(GMSMapView *)mapView didLongPressAtCoordinate:(CLLocationCoordinate2D)coordinate {
    NSLog(@"You tapped at %f,%f", coordinate.latitude, coordinate.longitude);
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.groundAnchor = CGPointMake(0.5, 1);
    marker.icon = [UIImage imageNamed:@"Marker"];
    marker.title = @"ตำแหน่ง";
    marker.snippet = [NSString stringWithFormat:@"พิกัด : %f,%f",coordinate.latitude,coordinate.longitude];
    marker.draggable = NO;
    marker.tappable = NO;
    marker.position = coordinate;
    marker.map = mapView_;
    
    [mapView_ setSelectedMarker:marker];
    self.remove.hidden = NO;
    

}


- (void)dealloc {
    [mapView_ removeObserver:self
                  forKeyPath:@"myLocation"
                     context:NULL];
}

#pragma mark - KVO updates

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    
    
    firstLocationUpdate_ = NO;
    
    CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
    
    
    NSLog(@"Lat %f ,Long %f",location.coordinate.latitude, location.coordinate.longitude);
    
    CLLocation *lastUpdatedLocation = location;
    CLLocationAccuracy desiredAccuracy = 100; // 100m accuracy
    CLLocationAccuracy desiredAccuracy30 = 30; // 100m accuracy
    NSLog(@"Accuracy : %f",lastUpdatedLocation.horizontalAccuracy);
    
    CATransition *animation = [CATransition animation];
    animation.duration = 1.5;
    animation.type = kCATransitionFade;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [label_point.layer addAnimation:animation forKey:@"changeLabelTransition"];
    [view_place.layer addAnimation:animation forKey:@"changeTextTransition"];
    
    
    // Accuracy มากกว่า 30m น้อยกว่า 100m Orange Color
    if (lastUpdatedLocation.horizontalAccuracy >= desiredAccuracy30 && lastUpdatedLocation.horizontalAccuracy < desiredAccuracy) {
        
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        [geocoder reverseGeocodeLocation:lastUpdatedLocation
                       completionHandler:^(NSArray *placemarks, NSError *error) {
                           
                           NSLog(@"placemarks=%@",[placemarks objectAtIndex:0]);
                           CLPlacemark *placemark = [placemarks objectAtIndex:0];
                           
                           label_point.text = [NSString stringWithFormat:@"พิกัด : %f,%f (acc %.2f)",location.coordinate.latitude,location.coordinate.longitude,lastUpdatedLocation.horizontalAccuracy];
                           [label_point setTextColor:[UIColor whiteColor]];
                           [label_point setBackgroundColor:[UIColor orangeColor]];
                           
                           NSMutableString *strLblTexts = [[NSMutableString alloc] init];
                           
                           
                           if (placemark.subLocality != nil) {
                               if ([strLblTexts isEqualToString:@""]||[strLblTexts isEqual:nil]) {
                                   [strLblTexts appendString:placemark.subLocality];
                               }
                               else{
                                   NSString *strtemp=[NSString stringWithFormat:@",%@",placemark.subLocality];
                                   
                                   NSLog(@"ตำบล :%@", strtemp);
                                   [strLblTexts appendString:strtemp];
                               }
                           }if (placemark.locality != nil) {
                               if ([strLblTexts isEqualToString:@""]||[strLblTexts isEqual:nil]) {
                                   [strLblTexts appendString:placemark.locality];
                               }
                               else{
                                   NSString *strtemp=[NSString stringWithFormat:@",%@",placemark.locality];
                                   
                                   NSLog(@"อำเภอ :%@", strtemp);
                                   [strLblTexts appendString:strtemp];
                               }
                           }if (placemark.subAdministrativeArea != nil) {
                               if ([strLblTexts isEqualToString:@""]||[strLblTexts isEqual:nil]) {
                                   [strLblTexts appendString:placemark.subAdministrativeArea];
                               }
                               else{
                                   NSString *strtemp=[NSString stringWithFormat:@",%@",placemark.subAdministrativeArea];
                                   
                                   NSLog(@"จังหวัด :%@", strtemp);
                                   [strLblTexts appendString:strtemp];
                               }
                           }if (placemark.country != nil) {
                               if ([strLblTexts isEqualToString:@""]||[strLblTexts isEqual:nil]) {
                                   [strLblTexts appendString:placemark.country];
                               }
                               else{
                                   NSString *strtemp=[NSString stringWithFormat:@",\n%@",placemark.country];
                                   
                                   NSLog(@"ประเทศ :%@", strtemp);
                                   [strLblTexts appendString:strtemp];
                               }
                           }if (placemark.postalCode != nil) {
                               if ([strLblTexts isEqualToString:@""]||[strLblTexts isEqual:nil]) {
                                   [strLblTexts appendString:placemark.postalCode];
                               }
                               else{
                                   NSString *strtemp=[NSString stringWithFormat:@",%@",placemark.postalCode];
                                   
                                   NSLog(@"ไปษณีย์ :%@", strtemp);
                                   [strLblTexts appendString:strtemp];
                               }
                           }
                           
                           view_place.text = strLblTexts;
                           [view_place setTextColor:[UIColor whiteColor]];
                           [view_place setBackgroundColor:[UIColor orangeColor]];
//                           [mview addSubview:label_point];
//                           [mview addSubview:view_place];
                           
                       }];
        
        
        // Accuracy มากกว่าหรือเท่ากับ 100m RED Color
    }else if (lastUpdatedLocation.horizontalAccuracy >= desiredAccuracy) {
        
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        [geocoder reverseGeocodeLocation:lastUpdatedLocation
                       completionHandler:^(NSArray *placemarks, NSError *error) {
                           
                           NSLog(@"placemarks=%@",[placemarks objectAtIndex:0]);
                           CLPlacemark *placemark = [placemarks objectAtIndex:0];
                           
                           
                           label_point.text = [NSString stringWithFormat:@"พิกัด : %f,%f (acc %.2f)",location.coordinate.latitude,location.coordinate.longitude,lastUpdatedLocation.horizontalAccuracy];
                           [label_point setTextColor:[UIColor whiteColor]];
                           [label_point setBackgroundColor:[UIColor redColor]];
                           
                           
                           NSMutableString *strLblTexts = [[NSMutableString alloc] init];
                           
                           
                           if (placemark.subLocality != nil) {
                               if ([strLblTexts isEqualToString:@""]||[strLblTexts isEqual:nil]) {
                                   [strLblTexts appendString:placemark.subLocality];
                               }
                               else{
                                   NSString *strtemp=[NSString stringWithFormat:@",%@",placemark.subLocality];
                                   
                                   NSLog(@"ตำบล :%@", strtemp);
                                   [strLblTexts appendString:strtemp];
                               }
                           }if (placemark.locality != nil) {
                               if ([strLblTexts isEqualToString:@""]||[strLblTexts isEqual:nil]) {
                                   [strLblTexts appendString:placemark.locality];
                               }
                               else{
                                   NSString *strtemp=[NSString stringWithFormat:@",%@",placemark.locality];
                                   
                                   NSLog(@"อำเภอ :%@", strtemp);
                                   [strLblTexts appendString:strtemp];
                               }
                           }if (placemark.subAdministrativeArea != nil) {
                               if ([strLblTexts isEqualToString:@""]||[strLblTexts isEqual:nil]) {
                                   [strLblTexts appendString:placemark.subAdministrativeArea];
                               }
                               else{
                                   NSString *strtemp=[NSString stringWithFormat:@",%@",placemark.subAdministrativeArea];
                                   
                                   NSLog(@"จังหวัด :%@", strtemp);
                                   [strLblTexts appendString:strtemp];
                               }
                           }if (placemark.country != nil) {
                               if ([strLblTexts isEqualToString:@""]||[strLblTexts isEqual:nil]) {
                                   [strLblTexts appendString:placemark.country];
                               }
                               else{
                                   NSString *strtemp=[NSString stringWithFormat:@",\n%@",placemark.country];
                                   
                                   NSLog(@"ประเทศ :%@", strtemp);
                                   [strLblTexts appendString:strtemp];
                               }
                           }if (placemark.postalCode != nil) {
                               if ([strLblTexts isEqualToString:@""]||[strLblTexts isEqual:nil]) {
                                   [strLblTexts appendString:placemark.postalCode];
                               }
                               else{
                                   NSString *strtemp=[NSString stringWithFormat:@",%@",placemark.postalCode];
                                   
                                   NSLog(@"ไปษณีย์ :%@", strtemp);
                                   [strLblTexts appendString:strtemp];
                               }
                           }
                           
                           
                           view_place.text = strLblTexts;
                           [view_place setTextColor:[UIColor whiteColor]];
                           [view_place setBackgroundColor:[UIColor redColor]];
//                           [mview addSubview:label_point];
//                           [mview addSubview:view_place];
                           
                       }];
        
        
        // Accuracy น้อยกว่า 30m Green Color
    }else if (lastUpdatedLocation.horizontalAccuracy < desiredAccuracy30) {
        
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        [geocoder reverseGeocodeLocation:lastUpdatedLocation
                       completionHandler:^(NSArray *placemarks, NSError *error) {
                           
                           NSLog(@"placemarks=%@",[placemarks objectAtIndex:0]);
                           CLPlacemark *placemark = [placemarks objectAtIndex:0];
                           
                           
                           label_point.text  = [NSString stringWithFormat:@"พิกัด : %f,%f (acc %.2f)",location.coordinate.latitude,location.coordinate.longitude,lastUpdatedLocation.horizontalAccuracy];
                           
                           [label_point setTextColor:[UIColor whiteColor]];
                           [label_point setBackgroundColor:[UIColor greenColor]];
                           
                           
                           NSMutableString *strLblTexts = [[NSMutableString alloc] init];
                           
                           
                           if (placemark.subLocality != nil) {
                               if ([strLblTexts isEqualToString:@""]||[strLblTexts isEqual:nil]) {
                                   [strLblTexts appendString:placemark.subLocality];
                               }
                               else{
                                   NSString *strtemp=[NSString stringWithFormat:@",%@",placemark.subLocality];
                                   
                                   NSLog(@"ตำบล :%@", strtemp);
                                   [strLblTexts appendString:strtemp];
                               }
                           }if (placemark.locality != nil) {
                               if ([strLblTexts isEqualToString:@""]||[strLblTexts isEqual:nil]) {
                                   [strLblTexts appendString:placemark.locality];
                               }
                               else{
                                   NSString *strtemp=[NSString stringWithFormat:@",%@",placemark.locality];
                                   
                                   NSLog(@"อำเภอ :%@", strtemp);
                                   [strLblTexts appendString:strtemp];
                               }
                           }if (placemark.subAdministrativeArea != nil) {
                               if ([strLblTexts isEqualToString:@""]||[strLblTexts isEqual:nil]) {
                                   [strLblTexts appendString:placemark.subAdministrativeArea];
                               }
                               else{
                                   NSString *strtemp=[NSString stringWithFormat:@",%@",placemark.subAdministrativeArea];
                                   
                                   NSLog(@"จังหวัด :%@", strtemp);
                                   [strLblTexts appendString:strtemp];
                               }
                           }if (placemark.country != nil) {
                               if ([strLblTexts isEqualToString:@""]||[strLblTexts isEqual:nil]) {
                                   [strLblTexts appendString:placemark.country];
                               }
                               else{
                                   NSString *strtemp=[NSString stringWithFormat:@",\n%@",placemark.country];
                                   
                                   NSLog(@"ประเทศ :%@", strtemp);
                                   [strLblTexts appendString:strtemp];
                               }
                           }if (placemark.postalCode != nil) {
                               if ([strLblTexts isEqualToString:@""]||[strLblTexts isEqual:nil]) {
                                   [strLblTexts appendString:placemark.postalCode];
                               }
                               else{
                                   NSString *strtemp=[NSString stringWithFormat:@",%@",placemark.postalCode];
                                   
                                   NSLog(@"ไปษณีย์ :%@", strtemp);
                                   [strLblTexts appendString:strtemp];
                               }
                           }
                           
                           
                           view_place.text = strLblTexts;
                           [view_place setTextColor:[UIColor whiteColor]];
                           [view_place setBackgroundColor:[UIColor greenColor]];
                           
//                           [mview addSubview:label_point];
//                           [mview addSubview:view_place];
                           
                       }];
        
        
    }
    
    //                label_point.text = [NSString stringWithFormat:@"รอสักครู่เพื่อให้พิกัดแม่นยำ..."];
    //                [label_point setTextColor:[UIColor whiteColor]];
    //                [label_point setBackgroundColor:[UIColor blueColor]];
    //
    //                view_place.text = [NSString stringWithFormat:@"เพื่อความรวดเร็วและความถูกต้องโปรดหลีกเลี่ยงการค้นหาภายในอาคาร"];
    //                [view_place setTextColor:[UIColor whiteColor]];
    //                [view_place setBackgroundColor:[UIColor blueColor]];
    //
    //                [mview addSubview:label_point];
    //                [mview addSubview:view_place];
    
    
}



@end