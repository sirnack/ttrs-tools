//
//  ViewController.m
//  TTRS Tool
//
//  Created by Prachaya Pan-in on 10/16/2558 BE.
//  Copyright © 2558 Prachaya Pan-in. All rights reserved.
//

#import "ViewController.h"
#import "MapLocateViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"วันที่ dd-MM-yyyy 'เวลา' HH:mm"];
    
    NSDate *date = [NSDate date];
    
//    NSString *formattedDateString = [dateFormatter stringFromDate:date];
    currentDate.text = [dateFormatter stringFromDate:date];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getYear {
    
   
}
- (IBAction)getMap:(id)sender {
    
        id controller = [[NSClassFromString(@"MapLocateViewController") alloc] initWithNibName:@"MapLocateViewController" bundle:nil];
        [self presentViewController:controller animated:YES completion:nil];
    
}
@end
