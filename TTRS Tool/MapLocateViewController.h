//
//  MapLocateViewController.h
//  TTRS Tool
//
//  Created by Prachaya Pan-in on 10/20/2558 BE.
//  Copyright © 2558 Prachaya Pan-in. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>
@interface MapLocateViewController : UIViewController<GMSMapViewDelegate>{
    
    
}
@property (nonatomic,strong) IBOutlet UIButton *close;
@property (nonatomic,strong) IBOutlet UIButton *remove;
@property (nonatomic,strong) IBOutlet UIView *mview;
@property (nonatomic,strong) IBOutlet UIView *mainview;
@property (nonatomic,strong) IBOutlet UIView *gmsview;
@property (nonatomic,strong) IBOutlet UILabel *label_point;
@property (nonatomic,strong) IBOutlet UITextView *view_place;
@property (nonatomic,strong) CLLocationManager *locationManager;

- (IBAction)removepin:(id)sender;
- (IBAction)closemap:(id)sender;
@end
